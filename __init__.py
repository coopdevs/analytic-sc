from trytond.pool import Pool

from .analytic_accounting_sc.lines_to_be_processed import LinesToBeProcessed
from .analytic_accounting_sc.wizards.create_lines_to_be_processed.create_lines_to_be_processed import \
        CreateLinesToBeProcessed
from .analytic_accounting_sc.wizards.create_lines_to_be_processed.start import CreateLinesToBeProcessedStart
from .analytic_accounting_sc.wizards.create_lines_to_be_processed.select_lines import \
        CreateLinesToBeProcessedSelectLines
from .analytic_accounting_sc.wizards.create_analytic_account_lines.create_analytic_account_lines import \
        CreateAnalyticAccountLines
from .analytic_accounting_sc.reports.analytic_account import AnalyticAccountReport
from .analytic_accounting_sc.analytic_account_account import AnalyticAccount


def register():
    Pool.register(LinesToBeProcessed,
                  CreateLinesToBeProcessedStart,
                  CreateLinesToBeProcessedSelectLines,
                  AnalyticAccount,
                  module="analytic_accounting_sc",
                  type_="model")

    Pool.register(CreateLinesToBeProcessed,
                  CreateAnalyticAccountLines,
                  module="analytic_accounting_sc",
                  type_="wizard")

    Pool.register(AnalyticAccountReport,
                  module="analytic_accounting_sc",
                  type_='report')
