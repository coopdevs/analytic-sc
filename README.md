# Tryton 3.8ish[1] module to manage analytic account

[1] The version of tryton 3.8 patched by NanTIC that we use.

## Python version

We are using [Pyenv](https://github.com/pyenv/pyenv) to fix the Python version and the virtualenv to test the package.

You need:

* Intall and configure [`pyenv`](https://github.com/pyenv/pyenv)
* Install and configure [`pyenv-virtualenvwrapper`](https://github.com/pyenv/pyenv-virtualenvwrapper)
* Intall locally the version of python needed:

```
$ pyenv install 2.7.9
```

* Create the virtualenv to use:

```
$ pyenv install 2.7.9
$ pyenv virtualenv 2.7.9 analytic_accounting_sc
```
