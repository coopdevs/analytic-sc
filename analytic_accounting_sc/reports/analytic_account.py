from trytond.modules.jasper_reports.jasper import JasperReport


class AnalyticAccountReport(JasperReport):
    __name__ = 'analytic.account.report'
