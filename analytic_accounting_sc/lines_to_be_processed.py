import logging

from decimal import Decimal

from trytond.model import ModelView, ModelSQL, fields, Unique

logger = logging.getLogger(__name__)


class LinesToBeProcessed(ModelView, ModelSQL):
    """Lines to be processed"""

    __name__ = 'lines.to.be.processed'

    account_move = fields.Many2One('account.move', 'Account Move', required=True, readonly=True)
    move_line = fields.Many2One('account.move.line', 'Account Move Line', required=True, readonly=True)
    analytic_account = fields.Many2One('analytic_account.account', 'Analytic Account', required=True, readonly=True)
    percentage = fields.Numeric('Percentage', required=True, digits=(3, 2))
    date = fields.Function(fields.Date('Date'), 'get_date')
    debit = fields.Function(fields.Numeric('Debit'), 'get_debit')
    credit = fields.Function(fields.Numeric('Credit'), 'get_credit')
    analytic_account_line = fields.Many2One('analytic_account.line', 'Analytic Account Line', readonly=True)

    @classmethod
    def __setup__(cls):
        super(LinesToBeProcessed, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('move_line_analytic_account_uniq', Unique(t, t.move_line, t.analytic_account),
                'Only one line is allowed per analytic account and move line.'),
            ]

    @staticmethod
    def default_percentage():
        return Decimal(100.00)

    def get_debit(self, name):
        if self.move_line:
            return self.move_line.debit

    def get_credit(self, name):
        if self.move_line:
            return self.move_line.credit

    def get_date(self, name):
        if self.move_line:
            return self.move_line.date
