from trytond.model import fields
from trytond.pool import PoolMeta


class AnalyticAccount:
    __metaclass__ = PoolMeta
    __name__ = 'analytic_account.account'

    lines = fields.One2Many('analytic_account.line', 'account', 'Lines')
