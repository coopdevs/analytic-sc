import logging

from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition

logger = logging.getLogger(__name__)


class CreateAnalyticAccountLines(Wizard):
    """ Create Analytic Account Lines"""
    __name__ = 'create.analytic.account.lines'
    start_state = 'open_'
    open_ = StateTransition()

    @staticmethod
    def _percentage(amount, percentage):
        return amount * (percentage/100)

    def _analytic_line_data(self, line):
        return {
            "name": "{} {}".format(line.move_line.description, line.analytic_account.name),
            "debit": self._percentage(line.debit, line.percentage),
            "credit": self._percentage(line.credit, line.percentage),
            "account": line.analytic_account,
            "move_line": line.move_line,
            "journal": line.account_move.journal,
            "date": line.date,
            "party": line.move_line.party,
            "company": line.move_line.account.company,
        }

    def transition_open_(self):
        LinesToBeProcessed = Pool().get('lines.to.be.processed')
        AnalyticAccountLine = Pool().get('analytic_account.line')
        lines_ids = Transaction().context['active_ids']

        for line in LinesToBeProcessed.browse(lines_ids):
            if line.analytic_account_line:
                continue
            analytic_line = AnalyticAccountLine(**self._analytic_line_data(line))
            analytic_line.save()
            line.analytic_account_line = analytic_line.id
            line.save()
        return 'end'
