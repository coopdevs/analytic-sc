import logging

from decimal import Decimal

from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition
from trytond.wizard import Button

logger = logging.getLogger(__name__)


class CreateLinesToBeProcessed(Wizard):
    """ Create Lines to be processed"""
    __name__ = 'create.lines.to.be.processed'
    start = StateView(
        'create.lines.to.be.processed.start',
        'analytic_accounting_sc.create_lines_to_be_processed_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'select_lines', 'tryton-ok', default=True)
        ]
    )
    select_lines = StateView(
        'create.lines.to.be.processed.select_lines',
        'analytic_accounting_sc.create_lines_to_be_processed_select_lines',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'create_lines', 'tryton-ok', default=True)
        ]
    )
    create_lines = StateTransition()

    def default_select_lines(self, fields):
        moves = Pool().get('account.move').search([
            ('journal', '=', self.start.journal),
            ('date', '>=', self.start.start_date),
            ('date', '<=', self.start.end_date),
            ('state', '=', 'posted')
            ])
        lines = []
        for move in moves:
            for line in move.lines:
                lines.append(line.id)
        return {
                'lines': lines
                }

    def _line_data(self, line):
        return {
                "account_move": line.move.id,
                "move_line": line.id,
                "analytic_account": self.start.analytic_account.id,
                "percentage": Decimal(100.00)
            }

    def transition_create_lines(self):
        LinesToBeProcessed = Pool().get('lines.to.be.processed')
        map(lambda line: LinesToBeProcessed(**self._line_data(line)).save(), self.select_lines.lines)
        return 'end'
