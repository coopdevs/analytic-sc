from trytond.model import ModelView, fields


class CreateLinesToBeProcessedSelectLines(ModelView):
    """ Create Lines to be processed select lines """
    __name__ = 'create.lines.to.be.processed.select_lines'
    lines = fields.One2Many('account.move.line', None, 'Lines')
