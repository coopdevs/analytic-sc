from trytond.model import ModelView, fields


class CreateLinesToBeProcessedStart(ModelView):
    """ Create Lines to be processed start """
    __name__ = 'create.lines.to.be.processed.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    journal = fields.Many2One('account.journal', 'Journal', required=True)
    analytic_account = fields.Many2One('analytic_account.account', 'Analytic Account', required=True)
